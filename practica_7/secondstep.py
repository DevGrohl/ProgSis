import sys

class secondstep():
    def __init__(self):
        pass

    def tokenize(self, operator):
        separator = operator.find(",")
        first = operator[:separator]
        second = operator[separator+1:]
        return [first, second]

    def bit_invertion(self, binary):
        result = ""
        for bit in binary:
            if bit == "1":
                result += "0"
            else:
                result += "1"
        return result

    def second_step(self):
        out = open(sys.argv[1]+"tmp.txt", "r")
        directives = ['ORG', 'END', 'EQU',
                      'DB', 'DC.B', 'FCB', 
                      'DW', 'DC.W', 'FDB', 
                      'FCC',
                      'DS', 'DS.B', 'RMB',
                      'DS.W', 'RMW']
        for line in out:
            data = line.split()
            # print data
            codop = data[3]

            if codop not in directives:
                operator = data[4]
                machine_code = data[5]
                miss_bytes = data[6]
                direction_mode = data[7]

                register = {'X':"00", 'Y':"01", 'SP':"10", 'PC':"11"}
                pre_post_register = ['X+', 'Y+', 'SP+','+X', '+Y', '+SP', 'X-', 'Y-', 'SP-','-X', '-Y', '-SP']
                acumulator_register = {'A':"00", 'B':"01", 'D':"10"}

                tupla = self.tokenize(operator)


                if tupla[0] == '':
                    tupla[0] = '0'
                if tupla[0][0] == '[':
                    tupla[0] = tupla[0][1:]
                if tupla[1][-1] == ']':
                    tupla[1] = tupla[1][:-1]
                if tupla[0].isdigit():
                    tupla[0] = int(tupla[0])

                if direction_mode == "IDX":
                    if tupla[1] in pre_post_register:
                        inc_dec = ['+', '-']
                        p = "1"
                        if tupla[1][0] in inc_dec:
                            p = "0"
                        value = tupla[1].translate(None, '+-')
                        result = register[value]
                        result += "1"
                        result += p
                        if p == "0":
                            if tupla[1][0] == '+':
                                result += "{0:04b}".format(tupla[0]-1)
                            else:
                                result += "{0:04b}".format(16 - tupla[0])
                        else:
                            if tupla[1][-1] == '+':
                                result += "{0:04b}".format(tupla[0]-1)
                            else:
                                result += "{0:04b}".format(16 - tupla[0])
                        machine_code += "{0:02X}".format(int("0b" + result, 2))
                    else:
                        if tupla[0] in acumulator_register.keys():
                            result = "111"
                            result += register[tupla[1]]
                            result += "1"
                            result += acumulator_register[tupla[0]]
                            machine_code += "{0:02X}".format(int("0b" + result, 2))
                        else:
                            result = register[tupla[1]]
                            result += "0"
                            if int(tupla[0]) >= 0:
                                result += "{0:05b}".format(int(tupla[0]))
                            else:
                                tupla[0] = int(tupla[0])*-1
                                tupla[0] = "{0:05b}".format(tupla[0])
                                tupla[0] = self.bit_invertion(tupla[0])
                                tupla[0] = bin(int("0b"+tupla[0], 2)+1)
                                tupla[0] = "{0:05b}".format(int(tupla[0], 2))
                                result += tupla[0]
                            machine_code += "{0:02X}".format(int("0b" + result, 2))

                elif direction_mode == "IDX1":
                    result = "111"
                    result += register[tupla[1]]
                    if int(tupla[0]) >= 0:
                        result += "000"
                        result += "{0:08b}".format(tupla[0])
                    else:
                        result += "001"
                        tupla[0] = int(tupla[0])*-1
                        tupla[0] = "{0:08b}".format(tupla[0])
                        tupla[0] = self.bit_invertion(tupla[0])
                        tupla[0] = bin(int("0b"+tupla[0], 2)+1)
                        tupla[0] = "{0:08b}".format(int(tupla[0], 2))
                        if len(tupla[0]) > 8:
                            result = result[:-1] + tupla[0]
                        else:
                            result += tupla[0]
                    machine_code += "{0:02X}".format(int("0b" + result, 2))

                elif direction_mode == "IDX2":
                    result = "111"
                    result += register[tupla[1]]
                    result += "010"
                    machine_code += "{0:02X}".format(int("0b"+result, 2))
                    tupla[0] = int(tupla[0])
                    machine_code += "{0:02X}".format(tupla[0])

                elif direction_mode == "[IDX2]":
                    result = "111"
                    result += register[tupla[1]]
                    result += "011"
                    result = "{0:02X}".format(int("0b" + result, 2))
                    result += "{0:04X}".format(int(tupla[0]))
                    machine_code += result

                elif direction_mode == "[D,IDX]":
                    result = "111"
                    result += register[tupla[1]]
                    result += "111"
                    machine_code += "{0:02X}".format(int("0b" + result, 2))

                else:
                    pass

                print data[:5], "\t", machine_code