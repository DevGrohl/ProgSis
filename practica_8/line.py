from tag import *
from codop import *
from operator import *
import sys
import os

contloc = 0
dir_inic = 0
equ_value = 0
used_tags = []
org_once = False
machine_code = ""
direction_mode = ""
total_bytes = 0
miss_bytes = 0

class Line:
    t = Tag()
    c = Codop()
    o = Operator()
    register = [] 

    def __init__(self, s):
        self.line = s
        self.tokens = []

    def resetVariables(self):
        self.t.value = None
        self.c.value = None
        self.o.value = None

    def clear_machine_code(self):
        global machine_code
        y = ""
        for x in machine_code:
            if x != " ":
                y += x
        machine_code = y

    def tokenizer(self):
        self.resetVariables()
        checker = self.line.splitlines(True)                                                    #Returns a list with linebreaks and strings
        self.tokens = self.line.split()                                                         #List of strings without linebreaks

        error = False

        if ((self.line[0] == ' ' or self.line[0] == '\t') and len(self.tokens) > 2) or len(self.tokens) > 3 :
            string = ""
            for key in self.tokens[1:]:
                if key[0] != "\"" and key[-1] != "\"":
                    if key != " ":
                     string += key
                else:
                    string += key + " "
                self.tokens.remove(key)
            self.tokens.append(string)

        if checker == []:                                                                       #If there is an empty line
            pass
        else:
            if self.tokens == []:                                                               #If there is an empty token
                print "Los tokens no contienen caracteres alfabeticos\n"
            else:
                if checker[0][0] == ' ' or checker[0][0] == '\t' or checker[0][0] == '\r':      # Checker [0][0] is the first letter of the first string in the line
                                                                                                #if that first letter is a space, tabulation or return then the first token is not a tag
                    if len(self.tokens) == 2:                                                       #If there are TWO tokens
                        self.c.value = self.tokens[0]                                                   #The first one is a codop
                        self.o.value = self.tokens[1]                                                   #The second one is an operator
                        if self.c.validator() and self.o.validator():                                   #If both tokens pass throught their validators:
                            print "ETIQUETA : NULL" 
                            print "CODOP    : " + self.c.value
                            print "OPERANDO : " + self.o.value + "\n"
                    elif len(self.tokens) == 1:                                                     #If there is only ONE token
                        self.c.value = self.tokens[0]                                                   #Then that token is a codop
                        if self.c.validator():                                                          #If the codop pass his validator
                            print "ETIQUETA : NULL"
                            print "CODOP   : " + self.c.value
                            print "OPERANDO : NULL\n"
                    else:                                                                           #If there ammount of tokens is not TWO or ONE
                        print "L_ERROR: Numero de argumentos incorrectos\n"     
                   
                    if self.c.value == "END":                                                       #If the codop is END...
                        # exit(0)   
                        return 0                                                                      #Finish the script
                else:                                                                           #If the first letter wasn't a space, tabulation or return:
                    if self.tokens[0] == "END":                                                 #if the tag was END is not valid since END must be a codop
                        print "L_ERROR: Codop invalido\n"

                    if len(self.tokens) == 2:                                                   #If there are TWO tokens...
                        self.t.value = self.tokens[0]                                               #The first one is a tag
                        self.c.value = self.tokens[1]                                               #The second one is a codop
                        if self.t.validator() and self.c.validator():                               #If both tokens pass throught their validators:
                            print "ETIQUETA : " + self.t.value
                            print "CODOP    : " + self.c.value 
                            print "OPERANDO : NULL\n"   
                    elif len(self.tokens) == 3:                                                 #If there are THREE tokens...
                        self.t.value = self.tokens[0]                                               #The first one is a tag
                        self.c.value = self.tokens[1]                                               #The second one is a codop
                        self.o.value = self.tokens[2]                                               #The third one is an operator
                        if self.t.validator() and self.c.validator() and self.o.validator():            #If all the tokens pass throught their validators:
                            print "ETIQUETA: " + self.t.value
                            print "CODOP   : " + self.c.value
                            print "OPERANDO: " + self.o.value + "\n"
                    else:                                                                       #If the ammount of tokens isn't TWO or THREE
                        print "L_ERROR: Numero de argumentos incorrectos\n"
                        error=True

                if error == True:
                    return 0
    #End Tokenizer

    def codop_evaluation(self):
        global total_bytes
        global miss_bytes
        global machine_code
        global org_once 
        global contloc
        global equ_value
        global dir_inic
        global direction_mode
        global used_tags

        equ_value = contloc

        directive = 0
        directives = {'ORG':1, 'END':2, 'EQU':3,
                      'DB':4, 'DC.B':4, 'FCB':4, 
                      'DW':5, 'DC.W':5, 'FDB':5, 
                      'FCC':6,
                      'DS':7, 'DS.B':7, 'RMB':7,
                      'DS.W':8, 'RMW':8}

        out = open(sys.argv[1]+"tmp.txt", "a")

        if self.c.value.upper() in directives:

            directive = directives[self.c.value.upper()]

            if directive == 1:
                if not org_once:
                    org_once = True
                    if self.o.value != None:
                        if self.o.get_value() != None:
                            dir_inic = self.o.get_value()
                            contloc = dir_inic
                            if dir_inic in range(0, 65536):
                                out.write("DIR_INIC\t {0:04x} \t %s \t %s \t %s\n".format(dir_inic) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            else:
                                print "CO_Error: El operador asociado a ORG esta fuera del limite [0 - 65535]\n"
                    else:
                        print "C_Error: El codop ORG necesita un operador asociado [0 - 65535]\n"
                else:
                    print "C_Error: Existe mas de un codop ORG en el archivo\n"
            elif directive == 2:
                out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                print "- - - - - - - - - - - - - - - - - - - "
            elif directive == 3:
                if self.t.value != None:
                    if not self.t.value in used_tags:
                        if self.o.value != None:
                            if self.o.get_value() != None:
                                equ_value = self.o.get_value()
                                if equ_value in range (0, 65536):
                                    out.write("VALOR_EQU\t {0:04x} \t %s \t %s \t %s\n".format(equ_value) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                                else:
                                    print "CO_Error: El operador asociado a EQU esta fuera del rango [0 - 65535]\n"
                        else:
                            print "CO_Error: El codop EQU necesita un operador asociado\n"
                else:
                    print "CT_Error: El codop EQU necesita una etiqueta asociada\n"
            elif directive == 4:
                if self.o.value != None:
                    if self.o.get_value():
                        if self.o.get_value() in range(0,256):
                            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            contloc += 1
                        else:
                            print "CO_Error: El operador asociado a la directiva esta fuera del rango [0 - 255]\n"
                else:
                    print "O_Error: La directiva necesita un operador asociado\n"
            elif directive == 5:
                if self.o.value != None:
                    if self.o.get_value():
                        if self.o.get_value() in range(0,65536):
                            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            contloc += 2
                        else:
                            print "CO_Error: El operador asociado a la directiva esta fuera del rango [0 - 65535]\n"
                else:
                    print "O_Error: La directiva necesita un operador asociado\n"
            elif directive == 6:
                if self.o.value != None:
                    if len(self.o.value) > 2:
                        print self.o.value[0] , " ", self.o.value[-1]
                        if self.o.value[0] == "\"" and self.o.value[-1] == "\"":
                            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            contloc += len(self.o.value) - 2  #One more for the \n
                            print contloc, " -------", len(self.o.value) - 2
                        else:
                            print "CO_Error: El operador asociado no esta bien formado \"cadena\"\n"
                    else:
                        print "CO_Error: El operador asociado no esta bien formado \"cadena\"\n"
                else:
                    print "CO_Error: La directiva necesita un operador asociado\n"
            elif directive == 7:
                if self.o.value != None:
                    if self.o.get_value():
                        op_value = self.o.get_value()
                        if op_value in range(0, 65536):
                            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            contloc += op_value * 1
                        else:
                            print "CO_Error: El operador asociado a la directiva esta fuera del rango [0 - 65535]\n"
                else:
                    print "CO_Error: La directiva necesita un operador asociado\n"
            elif directive == 8: 
                if self.o.value != None:
                    if self.o.get_value():
                        op_value = self.o.get_value()
                        if op_value in range(0, 65536):
                            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s\n".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
                            contloc += op_value * 2
                        else:
                            print "CO_Error: El operador asociado a la directiva esta fuera del rango [0 - 65535]\n"
                else:
                    print "CO_Error: La directiva necesita un operador asociado\n"
        else:
            out.write("CONTLOC\t\t {0:04x} \t %s \t %s \t %s".format(contloc) % (str(self.t.value), str(self.c.value), str(self.o.value)))
            equ_value = contloc
            operator_type = self.o.analizer()
            tabop = self.c.evaluator()
            found = False

            if tabop != []:
                if tabop[0][1] == "NO" and self.o.value != None:
                    print "L_Error: Codop contiene operador que no deberia\n"
                elif tabop[0][1] == "SI" and self.o.value == None:
                    print "L_Error: Codop no contiene operador que deberia\n"
                else:
                    error_flag = False
                    for reg in tabop:
                        if reg[2] == "INH" and operator_type == 0:
                            print "Inherente de", reg[6].strip(), "bytes" 
                            found = True
                        elif reg[2] == "IMM":

                            if operator_type == 1:
                                print "Inmediato de",
                                if reg[5] == "1":
                                    print "8 bits,",
                                elif reg[5] == "2":
                                    print "16 bits",
                                print "de", reg[6].strip(), "bytes" 
                                found = True

                            elif operator_type == 2:
                                print "Inmediato de",
                                if reg[5] == "1":
                                    print "8 bits,",
                                elif reg[5] == "2":
                                    print "16 bits",
                                print "de", reg[6].strip(), "bytes" 
                                found = True                                
                        elif reg[2] == "DIR" and operator_type == 3:
                            print "Directo de", reg[6].strip(), "bytes" 
                            found = True
                        elif reg[2] == "EXT" and operator_type == 4:
                            print "Extendido de", reg[6].strip(), "bytes"
                            found = True
                        elif operator_type == 5:
                            if reg[2] == "REL": 
                                print "Relativo",
                                if reg[6].strip() == "2":
                                    print "8 bits,",
                                elif reg[6].strip() == "4":
                                    print "16 bits,",
                                print"de", reg[6].strip(), "bytes" 
                                found = True
                            elif reg[2] == "EXT":
                                print "Extendido","de", reg[6].strip(), "bytes"
                                found = True
                        elif reg[2] == "IDX":
                            if operator_type == 6:
                                print "Indexado de 5 bits, de", reg[6].strip(), "bytes" 
                                found = True
                            elif operator_type == 10:
                                print "de", reg[6].strip(), "bytes" 
                                found = True
                            elif operator_type == 11:
                                print "Indexado de acumulador, de", reg[6].strip(), "bytes" 
                                found = True
                        elif reg[2] == "IDX1" and operator_type == 7:
                            print "Indexado de 9 bits, de", reg[6].strip(), "bytes" 
                            found = True
                        elif reg[2] == "IDX2" and operator_type == 8:
                            print "Indexado de 16 bits, de", reg[6].strip(), "bytes" 
                            found = True
                        elif reg[2] == "[IDX2]" and operator_type == 9:
                            print "de", reg[6].strip(), "bytes" 
                            found = True
                        elif reg[2] == "[D,IDX]" and operator_type == 12:
                            print "Indexado de acumulador indirecto, de", reg[6].strip(), "bytes" 
                            found = True
                        else:
                            error_flag = True

                        if found :
                            register = reg
                            break

                        # print "Modo de direccionamiento: ", reg[2]
                        # print "Codigo maquina calculado: ", reg[3]
                        # print "Total de bytes calculado: ", reg[4]
                        # print "Total de bytes no calculados: ", reg[5]
                        # print "Suma total de bytes: ", reg[6]

                    print ""

                    error_flag = False
            else:
                print "Error, codop no encontrado\n"

            contloc += int(register[6])
            total_bytes = int(reg[6])
            miss_bytes = int(reg[5])
            machine_code = reg[3]
            direction_mode = reg[2]

        #Imprimir a temporal y tabsim
        out = open("tabsim.txt", "a")
        if self.t.value != None:
            if not self.t.value in used_tags:
                if self.o.value != None and self.o.value.isdigit():
                    if self.o.get_value() in range (0,65536):
                        if self.c.value.upper() == "EQU":
                            out.write("VALOR_EQU\t %s \t {0:04x} \n".format(equ_value) % str(self.t.value))
                        else:
                            out.write("CONTLOC\t\t %s \t {0:04x} \n".format(equ_value) % str(self.t.value))        
                        used_tags.append(str(self.t.value))
                    else:
                        print "CT_Error: el operador esta fuera de rango [0 - 65535]"
                else:
                    if self.c.value.upper() == "EQU":
                        out.write("VALOR_EQU\t %s \t {0:04x} \n".format(equ_value) % str(self.t.value))
                    else:
                        out.write("CONTLOC\t\t %s \t {0:04x} \n".format(equ_value) % str(self.t.value))        
                    used_tags.append(str(self.t.value))
            else:
                print "CT_Error: La etiqueta ya fue utilizada anteriormente\n"

        self.clear_machine_code()

        out = open(sys.argv[1]+"tmp.txt", "a")
        if self.o.value != None and self.c.value not in directives: 
            if direction_mode != "IDX" and direction_mode != "IDX1" and direction_mode != "IDX2" and direction_mode != "[IDX2]" and direction_mode != "[D,IDX]" and direction_mode != "REL":
                if total_bytes == 1:
                    out.write("\t\t%s \t\t 0 \t DIR\n" % machine_code)
                elif total_bytes == 2:
                    if miss_bytes == 1:
                        out.write("\t\t%s{0:02x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:2], miss_bytes, direction_mode))
                    if miss_bytes == 0:
                        out.write("\t\t%s{0:02x} \t 0  \t %s\n".format(self.o.get_value()) % (machine_code[:2], direction_mode))
                elif total_bytes == 3:
                    if miss_bytes == 1:
                        out.write("\t\t%s{0:02x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s{0:04x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 4:
                    if miss_bytes == 1:
                        out.write("\t\t%s{0:02x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s{0:04x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s{0:06x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 5:
                    if miss_bytes == 1:
                        out.write("\t\t%s{0:02x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:8], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s{0:04x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s{0:06x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 4:
                        out.write("\t\t%s{0:08x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 6:
                    if miss_bytes == 1:
                        out.write("\t\t%s{0:02x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:10], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s{0:04x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:8], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s{0:06x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 4:
                        out.write("\t\t%s{0:08x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 5:
                        out.write("\t\t%s{0:10x} \t %s \t %s\n".format(self.o.get_value()) % (machine_code[:2], miss_bytes, direction_mode))
            else:
                if total_bytes == 1:
                    out.write("\t\t%s\n" % machine_code)
                elif total_bytes == 2:
                    if miss_bytes == 1:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 3:
                    if miss_bytes == 1:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 4:
                    if miss_bytes == 1:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 5:
                    if miss_bytes == 1:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:8], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 4:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:2], miss_bytes, direction_mode))
                elif total_bytes == 6:
                    if miss_bytes == 1:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:10], miss_bytes, direction_mode))
                    elif miss_bytes == 2:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:8], miss_bytes, direction_mode))
                    elif miss_bytes == 3:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:6], miss_bytes, direction_mode))
                    elif miss_bytes == 4:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:4], miss_bytes, direction_mode))
                    elif miss_bytes == 5:
                        out.write("\t\t%s \t %s \t %s\n" % (machine_code[:2], miss_bytes, direction_mode))
            
        elif self.o.value == None and self.c.value not in directives:
            out.write("\t\t%s \t 0 \t INH\n" %machine_code)
        machine_code =""
        # out.write("\n")


    def result(self):
        global contloc
        global dir_inic
        return "Longitud total en bytes: " + str(contloc - dir_inic) + "\n"
