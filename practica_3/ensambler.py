#!/usr/bin/python2.7
from line import *

# f = open("P3ASM.txt", 'r+')
f = open("P3BASM.txt", 'r+')
# f = open("tronador03.asm", 'r+')
for line in f:                      #For each line in the file
    limit = line.find(';')          #Find if that line its a comment
    if limit != -1:                 #If the limit had a comment.. 
        print "COMENTARIO\n"        #print it
    else:
        limit = len(line)
        l = Line(line[:limit])          #Create an instance of Line from the beginning to the limit of the line
        l.tokenizer()                   #Tokenize the line