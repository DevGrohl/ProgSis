from tag import *
from codop import *
from operator import *

class Line:
    t = Tag()
    c = Codop()
    o = Operator()

    def __init__(self, s):
        self.line = s
        self.tokens = []

    def resetVariables(self):
        self.t.value = None
        self.c.value = None
        self.o.value = None

    def tokenizer(self):
        checker = self.line.splitlines(True)                                                    #Returns a list with linebreaks and strings
        self.tokens = self.line.split()                                                         #List of strings without linebreaks

        error = False
        if ((self.line[0] == ' ' or self.line[0] == '\t') and len(self.tokens) > 2) or len(self.tokens) > 3 :
            string = ""
            for key in self.tokens[1:]:
                # print key
                if key != " ":
                    string += key
                # print string
                self.tokens.remove(key)
            self.tokens.append(string)

        # print self.tokens

        if checker == []:                                                                       #If there is an empty line
            pass
        else:
            if self.tokens == []:                                                               #If there is an empty token
                print "Los tokens no contienen caracteres alfabeticos\n"
            else:
                if checker[0][0] == ' ' or checker[0][0] == '\t' or checker[0][0] == '\r':      # Checker [0][0] is the first letter of the first string in the line
                                                                                                #if that first letter is a space, tabulation or return then the first token is not a tag
                    if len(self.tokens) == 2:                                                       #If there are TWO tokens
                        self.c.value = self.tokens[0]                                                   #The first one is a codop
                        self.o.value = self.tokens[1]                                                   #The second one is an operator
                        if self.c.validator() and self.o.validator():                                   #If both tokens pass throught their validators:
                            print "ETIQUETA : NULL" 
                            print "CODOP    : " + self.c.value
                            print "OPERANDO : " + self.o.value + "\n"
                    elif len(self.tokens) == 1:                                                     #If there is only ONE token
                        self.c.value = self.tokens[0]                                                   #Then that token is a codop
                        if self.c.validator():                                                          #If the codop pass his validator
                            print "ETIQUETA : NULL"
                            print "CODOP   : " + self.c.value
                            print "OPERANDO : NULL\n"
                    else:                                                                           #If there ammount of tokens is not TWO or ONE
                        print "L_ERROR: Numero de argumentos incorrectos\n"     
                    if self.c.value == "END":                                                       #If the codop is END...
                        exit(0)                                                                         #Finish the script
                else:                                                                           #If the first letter wasn't a space, tabulation or return:
                    if self.tokens[0] == "END":                                                 #if the tag was END is not valid since END must be a codop
                        print "L_ERROR: Codop invalido\n"

                    if len(self.tokens) == 2:                                                   #If there are TWO tokens...
                        self.t.value = self.tokens[0]                                               #The first one is a tag
                        self.c.value = self.tokens[1]                                               #The second one is a codop
                        if self.t.validator() and self.c.validator():                               #If both tokens pass throught their validators:
                            print "ETIQUETA : " + self.t.value
                            print "CODOP    : " + self.c.value 
                            print "OPERANDO : NULL\n"   
                    elif len(self.tokens) == 3:                                                 #If there are THREE tokens...
                        self.t.value = self.tokens[0]                                               #The first one is a tag
                        self.c.value = self.tokens[1]                                               #The second one is a codop
                        self.o.value = self.tokens[2]                                               #The third one is an operator
                        if self.t.validator() and self.c.validator() and self.o.validator():            #If all the tokens pass throught their validators:
                            print "ETIQUETA: " + self.t.value
                            print "CODOP   : " + self.c.value
                            print "OPERANDO: " + self.o.value + "\n"
                    else:                                                                       #If the ammount of tokens isn't TWO or THREE
                        print "L_ERROR: Numero de argumentos incorrectos\n"
                        error=True

                if error == True:
                    return 0

                value_return = self.o.analizer()

                tabop = self.c.evaluator()

                if tabop != []:
                    #Practica 2
                    if tabop[0][1] == "NO" and self.o.value != None:
                        print "L_Error: Codop contiene operador que no deberia\n"
                    elif tabop[0][1] == "SI" and self.o.value == None:
                        print "L_Error: Codop no contiene operador que deberia\n"
                    else:
                        error_flag = False
                        for reg in tabop:
                            if reg[2] == "INH" and value_return == 0:
                                print "Inherente de", reg[6].strip(), "bytes" 
                            elif reg[2] == "IMM":

                                if value_return == 1:
                                    print "Inmediato de",
                                    if reg[5] == "1":
                                        print "8 bits,",
                                    elif reg[5] == "2":
                                        print "16 bits",
                                    print "de", reg[6].strip(), "bytes" 
                                elif value_return == 2:
                                    print "Inmediato de",
                                    if reg[5] == "1":
                                        print "8 bits,",
                                    elif reg[5] == "2":
                                        print "16 bits",
                                    print "de", reg[6].strip(), "bytes" 
                            elif reg[2] == "DIR" and value_return == 3:
                                print "Directo de", reg[6].strip(), "bytes" 
                            elif reg[2] == "EXT" and value_return == 4:
                                print "Extendido de", reg[6].strip(), "bytes" 
                            elif value_return == 5:
                                if reg[2] == "REL": 
                                    print "Relativo",
                                    if reg[6].strip() == "2":
                                        print "8 bits,",
                                    elif reg[6].strip() == "4":
                                        print "16 bits,",
                                    print"de", reg[6].strip(), "bytes" 
                                elif reg[2] == "EXT":
                                    print "Extendido","de", reg[6].strip(), "bytes"
                            elif reg[2] == "IDX":
                                if value_return == 6:
                                    print "Indexado de 5 bits, de", reg[6].strip(), "bytes" 
                                elif value_return == 10:
                                    print "de", reg[6].strip(), "bytes" 
                                elif value_return == 11:
                                    print "Indexado de acumulador, de", reg[6].strip(), "bytes" 
                            elif reg[2] == "IDX1" and value_return == 7:
                                print "Indexado de 9 bits, de", reg[6].strip(), "bytes" 
                            elif reg[2] == "IDX2" and value_return == 8:
                                print "Indexado de 16 bits, de", reg[6].strip(), "bytes" 
                            elif reg[2] == "[IDX2]" and value_return == 9:
                                print "de", reg[6].strip(), "bytes" 
                            elif reg[2] == "[D,IDX]" and value_return == 12:
                                print "Indexado de acumulador indirecto, de", reg[6].strip(), "bytes" 
                            else:
                                error_flag = True
                            # print ", de ","de", reg[6].strip(), "bytes" 

                            # print "Modo de direccionamiento: ", reg[2]
                            # print "Codigo maquina calculado: ", reg[3]
                            # print "Total de bytes calculado: ", reg[4]
                            # print "Total de bytes no calculados: ", reg[5]
                            # print "Suma total de bytes: ", reg[6]

                        print ""

                        # if error_flag == True:
                            # print error_flag
                            # print "El codido de operacion no maneja el tipo de operando que contiene.\n"

                        error_flag = False
                else:
                    print "Error, codop no encontrado\n"

                self.resetVariables()