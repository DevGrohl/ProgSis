#!/usr/bin/python2.7
import sys
from line import *
from secondstep import *

f = open(sys.argv[1]+".txt", 'r+')
for line in f:                      #For each line in the file
    limit = line.find(';')          #Find if that line its a comment
    if limit != -1:                 #If the limit had a comment.. 
        print "COMENTARIO\n"        #print it
    else:
        limit = len(line)
        l = Line(line[:limit])          #Create an instance of Line from the beginning to the limit of the line
        l.tokenizer()                   #Tokenize the line
        l.codop_evaluation()
        result = l.result()
print result
f.close()

s = secondstep()
s.second_step()