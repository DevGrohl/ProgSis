import sys
import os
import string
from operator import *

def base_evaluation(value):
    if value == None:
        return 0
    if value.isdigit():         #decimal base
        return 1            
    elif value[0] == '%':       #binary base
        return 2            
    elif value[0] == '@':       #octal base
        return 3            
    elif value[0] == '$':       #hexadecimal base
        return 4            
    elif value[0] == '#' or value[0] == '-':        #Inmediato
        if value[1:] != "":
            if value[1:].isdigit() or (value[1] == '-' and value[2:].isdigit()): #decimal base
                return 1
            elif value[1] == '%':                   #binary base
                return 2
            elif value[1] == '@':                   #octal base
                return 3
            elif value[1] == '$':                   #hexadecimal base
                return 4
        else:
            print "O_ERROR: El operando inmediato esta incompleto"
            return 9
    else:
        return 0

class secondstep():
    code = []

    def __init__(self):
        pass

    def tokenize(self, operator):
        separator = operator.find(",")
        first = operator[:separator]
        second = operator[separator+1:]
        return [first, second]

    def bit_invertion(self, binary):
        result = ""
        for bit in binary:
            if bit == "1":
                result += "0"
            else:
                result += "1"
        return result

    def second_step(self):
        out = open(sys.argv[1]+"tmp.txt", "r")
        temporal = []
        cont = 1
        directives = ['ORG', 'END', 'EQU',
                      'DB', 'DC.B', 'FCB', 
                      'DW', 'DC.W', 'FDB', 
                      'FCC',
                      'DS', 'DS.B', 'RMB',
                      'DS.W', 'RMW']
        for line in out:
            temporal.append(line)
        out.close()

        out = open(sys.argv[1]+"tmp.txt", "r")
        # for line, next_line in zip(out, temporal[1:]):
        for line in out:
            data = line.split()
            codop = data[3]
            if cont < len(temporal):
                next_line = temporal[cont]
                cont += 1
            next_l = next_line.split()
            next_contoloc = next_l[1]
            operator = Operator()
            operator.value = data[4]

            if codop not in directives:
                machine_code = data[5]
                miss_bytes = data[6]
                direction_mode = data[7]

                register = {'X':"00", 'Y':"01", 'SP':"10", 'PC':"11"}
                pre_post_register = ['X+', 'Y+', 'SP+','+X', '+Y', '+SP', 'X-', 'Y-', 'SP-','-X', '-Y', '-SP']
                acumulator_register = {'A':"00", 'B':"01", 'D':"10"}

                tupla = self.tokenize(operator.value)

                if tupla[0] == '':
                    tupla[0] = '0'
                if tupla[0][0] == '[':
                    tupla[0] = tupla[0][1:]
                if tupla[1][-1] == ']':
                    tupla[1] = tupla[1][:-1]
                if tupla[0].isdigit():
                    tupla[0] = int(tupla[0])

                if direction_mode == "IDX":
                    if tupla[1] in pre_post_register:
                        inc_dec = ['+', '-']
                        p = "1"
                        if tupla[1][0] in inc_dec:
                            p = "0"
                        value = tupla[1].translate(None, '+-')
                        result = register[value]
                        result += "1"
                        result += p
                        if p == "0":
                            if tupla[1][0] == '+':
                                result += "{0:04b}".format(tupla[0]-1)
                            else:
                                result += "{0:04b}".format(16 - tupla[0])
                        else:
                            if tupla[1][-1] == '+':
                                result += "{0:04b}".format(tupla[0]-1)
                            else:
                                result += "{0:04b}".format(16 - tupla[0])
                        machine_code += "{0:02X}".format(int("0b" + result, 2))
                    else:
                        if tupla[0] in acumulator_register.keys():
                            result = "111"
                            result += register[tupla[1]]
                            result += "1"
                            result += acumulator_register[tupla[0]]
                            machine_code += "{0:02X}".format(int("0b" + result, 2))
                        else:
                            result = register[tupla[1]]
                            result += "0"
                            if int(tupla[0]) >= 0:
                                result += "{0:05b}".format(int(tupla[0]))
                            else:
                                tupla[0] = int(tupla[0])*-1
                                tupla[0] = "{0:05b}".format(tupla[0])
                                tupla[0] = self.bit_invertion(tupla[0])
                                tupla[0] = bin(int("0b"+tupla[0], 2)+1)
                                tupla[0] = "{0:05b}".format(int(tupla[0], 2))
                                result += tupla[0]
                            machine_code += "{0:02X}".format(int("0b" + result, 2))

                elif direction_mode == "IDX1":
                    result = "111"
                    result += register[tupla[1]]
                    if int(tupla[0]) >= 0:
                        result += "000"
                        result += "{0:08b}".format(tupla[0])
                    else:
                        result += "001"
                        tupla[0] = int(tupla[0])*-1
                        tupla[0] = "{0:08b}".format(tupla[0])
                        tupla[0] = self.bit_invertion(tupla[0])
                        tupla[0] = bin(int("0b"+tupla[0], 2)+1)
                        tupla[0] = "{0:08b}".format(int(tupla[0], 2))
                        if len(tupla[0]) > 8:
                            result = result[:-1] + tupla[0]
                        else:
                            result += tupla[0]
                    machine_code += "{0:02X}".format(int("0b" + result, 2))

                elif direction_mode == "IDX2":
                    result = "111"
                    result += register[tupla[1]]
                    result += "010"
                    machine_code += "{0:02X}".format(int("0b"+result, 2))
                    tupla[0] = int(tupla[0])
                    machine_code += "{0:02X}".format(tupla[0])

                elif direction_mode == "[IDX2]":
                    result = "111"
                    result += register[tupla[1]]
                    result += "011"
                    result = "{0:02X}".format(int("0b" + result, 2))
                    result += "{0:04X}".format(int(tupla[0]))
                    machine_code += result

                elif direction_mode == "[D,IDX]":
                    result = "111"
                    result += register[tupla[1]]
                    result += "111"
                    machine_code += "{0:02X}".format(int("0b" + result, 2))

                elif direction_mode == "REL":
                    tabsim = open("tabsim.txt", "r")
                    value = ""
                    for reg in tabsim:
                        info = reg.split()
                        tag = info[1]
                        
                        if tag == operator.value:
                            value = info[2]
                    if value == "":
                        print "ERROR: La etiqueta solicitada no se encontro en el TABSIM"
                    else:
                        result = int(value, 16) - int(next_contoloc, 16)

                        if miss_bytes == "1":
                            if result in range(-128, 128):
                                if result < 0:
                                    result = result * -1
                                    result = "{0:08b}".format(result)
                                    result = self.bit_invertion(result)
                                    result = int("0b" + result, 2)+1
                                machine_code += "{0:02X}".format(result)
                            else:
                                print "Rango del desplazamiento no valido"
                        elif miss_bytes == "2":
                            if result in range (-32768, 32768):
                                if result < 0:
                                    result = result * -1
                                    result = "{0:016b}".format(result)
                                    result = self.bit_invertion(result)
                                    result = int("0b" + result, 2)+1
                                machine_code += "{0:04X}".format(result)
                            else:
                                print "Rango del desplazamiento no valido"
                
                elif direction_mode == "EXT":
                    tabsim = open("tabsim.txt", "r")
                    value = ""
                    if not base_evaluation(operator.value):                        
                        for reg in tabsim:
                            info = reg.split()
                            tag = info[1]
                            
                            if tag == operator.value:
                                value = info[2]
                        if value == "":
                            print "ERROR: La etiqueta solicitada no se encontro en el TABSIM"
                        else:
                            machine_code += "{0:04X}".format(int(value, 16))
                    else:
                        machine_code += "{0:04X}".format(operator.get_value())
                else:
                    pass

                print data[:5], "\t", machine_code
            else:
                machine_code = ""
                if codop in ["DB", "DC.B", "FCB"]:
                    machine_code = "{0:02X}".format(int(operator.get_value()))
                elif codop in ["DW", "DC.W", "FDB"]:
                    machine_code = "{0:04X}".format(int(operator.get_value()))
                elif codop == "FCC":
                    operator.value = ""
                    for fragment in data[4:]:
                        operator.value += fragment 
                        operator.value += " "
                    for char in operator.value[1:-2]:
                        machine_code += char.encode("hex")
                print data, "\t", machine_code
            self.code.append(machine_code)
        out.close()

    def get_initial(self, file, num):
        f = open(file+"tmp.txt", "r+")
        for i in range(num):
            line = f.readline()
        line = line.split()
        if  line[3] == "ORG":
            return None, None
        else:
            return line[1], line[3]

    def checksum(self, data):
        checksum = 0
        if all(c in string.hexdigits for c in data):
            data = [data[i:i+2] for i in range(0, len(data), 2)]
            for char in data:
                checksum += int(char, 16)
        else:
            for byte in data:
                checksum += ord(byte)

        checksum = bin(checksum)[2:]
        return checksum

    def header(self, file):
        register = "S0"
        # rute = os.getcwd()
        rute = "C:/"
        rute += file + ".txt" + "\n"
        register += "{0:02X}".format(len(rute)+3)
        register += "0000"

        for char in rute:
            register += str(char.encode("hex"))

        checksum = self.checksum(rute)
        checksum = bin(int(checksum,2)+len(rute)+3)
        checksum = checksum[2:]
        checksum = self.bit_invertion(checksum)
        checksum = "{0:04X}".format(int("0b" + checksum, 2))
        
        register += checksum[2:]
        return register

    def information(self, file):
        register = ""
        line = 1
        offset = 0
        while (len(self.code) > 0):
            file_info = self.get_initial(file, line)
            initial = file_info[0]
            codop = file_info[1]
            
            if self.code[0] == "":
                line += 1
                self.code.pop(0)
                continue

            if initial == None:
                line += 1
                self.code.pop(0)
                continue
            else:
                initial = int(initial,16)+offset
                initial = "{0:04X}".format(initial)
                offset = 0
                register += "S1"
                length = 0
                checksum = 0
                data = ""

            for codec in list(self.code):
                code = [codec[i:i+2] for i in range(0, len(codec), 2)]

                if length < 16:
                    codop = self.get_initial(file, line)[1]
                    line += 1


                    if code != [] or codop == "EQU":

                        for byte in list(code):
                            length += len(byte)/2
                            data += byte
                            code.remove(byte)

                            if length >= 16:

                                if len(code) > 0:
                                    offset = len(codec)/2
                                    for c in code :
                                        self.code.insert(0, c)
                                        offset -= 1
                                    line -= 1

                                break
                        self.code.remove(codec)

                    else:
                        self.code.remove(codec)
                        break
                else:
                    break

            length = "{0:02X}".format(length + 3)
            register += length
            register += initial
            register += data

            checksum = self.checksum(length + initial + data)
            checksum = "{0:016b}".format(int(checksum,2))
            checksum = self.bit_invertion(checksum)
            checksum = "{0:04X}".format(int("0b" + checksum, 2))
            register += checksum[2:] + "\n"
        return register[:-1]

    def final(self):
        register = "S9030000FC"
        return register

    def object_file(self, file):
        obj = open(file+".obj", "a")
        header = self.header(file)
        print header
        obj.write(header)
        print self.information(file)
        print self.final()
