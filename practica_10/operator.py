from tag import *
import string

operatos = ['%', '@', '#', '$']
binary = ['%','0','1']
octal = ['@','0','1','2','3','4','5','6','7']
hexadecimal = ['$','0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']

def base_evaluation(value):
    if value == None:
        return 0
    if value.isdigit():         #decimal base
        return 1            
    elif value[0] == '%':       #binary base
        return 2            
    elif value[0] == '@':       #octal base
        return 3            
    elif value[0] == '$':       #hexadecimal base
        return 4            
    elif value[0] == '#' or value[0] == '-':        #Inmediato
        if value[1:] != "":
            if value[1:].isdigit() or (value[1] == '-' and value[2:].isdigit()): #decimal base
                return 1
            elif value[1] == '%':                   #binary base
                return 2
            elif value[1] == '@':                   #octal base
                return 3
            elif value[1] == '$':                   #hexadecimal base
                return 4
        else:
            print "O_ERROR: El operando inmediato esta incompleto"
            return 9
    else:
        return 0

class Operator:
    def __init__(self, s=None):
        self.value = s
        self.reg = ""

    def tokenize(self):
        separator = self.value.find(",")
        first = self.value[:separator]
        second = self.value[separator+1:]
        return [first, second]

    def validator(self):
        return True

    def analizer(self):                                                             #Any lenght any character... to be updated (Updated)
        
        valid_registers = ('X', 'Y', 'SP', 'PC')                                    #Valid registers for Indexed modes
        acc_16b_flag = False                                                        #Flag for 16bits and accumulators '[]'
        idx_5b_flag = False                                                         #Flag for 5bits indexed
        string = ""
                
        if self.value != None and self.value.find(',') != -1:                       #If the operator have a comma, it's indexed and its tokenized
            tupla = self.tokenize()                                                 #Variable "tupla" takes two values (tupla[0], tupla[1])

            if tupla[1] == '':                                                      #If the second part its empty
                print "O_ERROR: Operando incompleto XXX,YYY"                        #Print error
                return None

            if tupla[0] == '':                                                      #If the first part its empty
                tupla[0] = "0"                                                      #Replace with a zero
                idx_5b_flag = True                                                  #This is only available for 5 bits indexed mode

            if tupla[0][0] == '[' or tupla[1][-1] == ']':                           #If the first char in the tuple its a '[' and the last its a ']'
                #Indirecto 16B or acumulador
                if tupla[0][0] != '[' or tupla[1][-1] != ']':                       #If there is a missing []
                    print "O_Error: el operador no esta bien formado [xxx, yyy]"    #Print error
                    return None
                else:
                    tupla[0] = tupla[0][1:]                                         #Rewrite for easier use
                    tupla[1] = tupla[1][:-1]                                        #Rewrite for easier use
                    acc_16b_flag = True                                             # "[]" its only available for accumulator and 16bits indirect
            
            tupla[1] = tupla[1].upper()                                             #Uppercase the second part of the tuple

            if tupla[0] not in('a', 'A', 'b', 'B', 'd', 'D') :                      #If the first tuple doesnt have a register from acumulators

                if tupla[0].find('[') == -1 and tupla[0].find(']') == -1:           #If there are not []
                    if tupla[0].isdigit():                                          #If first part is digit
                        tupla[0] = int(tupla[0])                                    #Cast to int the value of the tuple
                    elif tupla[0][1:].isdigit() and tupla[0][0] not in operatos:    
                        if tupla[0][0] == '-':
                            tupla[0] = int (tupla[0][1:]) * -1
                        else:
                            tupla[0] = int (tupla[0][1:])
                else:
                    tupla[0] = int(tupla[0][1:])

                if tupla[1] in valid_registers:                                     #If the second tuple its a valid register

                    if not isinstance(tupla[0], int):
                        if tupla[0][0] != '-':
                            print "O_ERROR: El primer registro no es base decimal"
                            return None

                    if acc_16b_flag:                                    #If the accumulator/16b flag its True
                        if tupla[0] in range (0, 65536):                #If the range of the register is between 0 and 65535
                            print "Indizado indirecto de 16 bits,",     #The operator is Indexed Indirect 16 bits
                            return 9
                        else:                                           #If is not in the range
                            print "Primer registro fuera de rango, [0 - 65535]"       #Print error
                            return None
                    if idx_5b_flag:                                     #If the Indexed 5 bits its True
                        if tupla[0] in range (-16, 16):                 #If the range of the register is between -16 and 15
                            return 6                                    #The operator is Indexed 5 bits
                        else:                                           #If is not in the range
                            print "Primer registro fuera de rango, [-16 - 15]"     #Print error
                            return None

                    if tupla[0] in range (-16, 16):                     #If the register is between -16 and 15
                        return 6                                        #The operator is Indexed 5 bits
                    elif (tupla[0] in range (-256, -17)) or (tupla[0] in range (16, 256)):      #If the register is between -256 and -17 OR 16 and 255
                        return 7                                        #The operator is Indexed 9 bits
                    elif tupla[0] in range (256, 65536):                #If the register is between 256 and 65535
                        return 8                                        #The operator is Indexed 16 bits
                    else:                                               #If the register is not between any range
                        print "El valor se encuentra fuera del rango, [-16 - 15] / [256 - 65535] / [-256 - -17 / 16 - 256]" #Print error
                        return None
                elif tupla[1] in ('X+', 'Y+', 'SP+','+X', '+Y', '+SP', 'X-', 'Y-', 'SP-','-X', '-Y', '-SP'):
                    if tupla[0] in range (1, 9):
                        if tupla[1] in ('X+', 'Y+', 'SP+'):
                            print "Indizado de post incremento,",
                        elif tupla[1] in ('+X', '+Y', '+SP'):
                            print "Indizado de pre incremento,",
                        elif tupla[1] in ('X-', 'Y-', 'SP-'):
                            print "Indizado de post decremento,",
                        elif tupla[1] in ('-X', '-Y', '-SP'):
                            print "Indizado de pre decremento,",
                        else:
                            print "O_ERROR: El segundo registro no es valido [+/- X/Y/SP' +/-]"
                            return None
                        return 10
                    else:                                                   #If the second tuple is not a valid register
                        print "O_Error: El primer registro esta fuera de rango [1,8]"               #Print error
                        return None
                elif tupla[1] not in valid_registers:
                    print "O_ERROR: El segundo registro no es valido, X/Y/SP/PC"
                    return None
            else:                                                       #Acumulators
                if acc_16b_flag:                                        #If the accumulator/16b flag its True
                    if tupla[1] in valid_registers:                     #If the second tuple is in the valid registers
                        if tupla[0] == 'D':                             #If the first tuple is the special register D
                            return 12                                   #The operator is Accumulator Indexed Indirect
                        else:                                           #If its not the special register D
                            print "Primer registro incorrecto, [D]"     #Print error
                            return None
                    else:                                               #If the second tuple is not a valid register
                        print "Segundo registro incorrecto X/Y/SP/PC"   #Print error
                        return None
                else:                                                   #Acumulator Indexed
                    if tupla[1] in valid_registers:                     #If the second tuple is a valid register
                        if tupla[0] in ('A', 'B', 'D'):                 #If the first tuple is a valid register for accumulator
                            return 11                                   #The operator is Accumulator Indexed
                        else:                                           #If the first tuple is not a valid register for accumulator
                            print "Primer registro incorrecto, A/B/D"           #Print error
                            return None
                    else:                                               #If the second tuple is not a valid register
                        print "Segundo registro incorrecto, X/Y/SP/PC"                #Print error
                        return 0
        elif self.value == None:                                            #If there is no operator
            return 0                                                 #The operator is Inherent
        elif self.value != None :                                                           #If its not Indexed nor Inherent
            base = base_evaluation(self.value)                          #The variable "base" takes a numeric value between 0 and 4

            if base == 9:
                return None

            if self.value.find('[') != -1 and self.value.find(']') != -1:
                if len(self.value) < 3:
                    print "O_ERROR: Operando incompleto [______]"
                    return None

            if base == 0:                                               #If the base is 0 the operator is a tag
                op_value = Tag()                                        #The variable "op_value" initialize as tag
                op_value.value = self.value                             #The property "value" of "op_value" takes the same value that the operator in the correct base
                if op_value.validator():                                #If the operator is a valid Tag
                    return 5
                else:
                    return None
            else:                                                       #If the base value is not 0
                op_value = self.get_value()                             #The variable "op_value" takes the same value that the operator in the correct base

            if op_value != False:
                if self.value[0] == '#':                                    #If the first character in the operator is "#"
                    # print "Inmediato de",                                 #The operator is Inmediate  
                    if int(op_value) in range (0, 256):                         #If the value of the operator is between 0 and 255
                        return 1                                            #The operator is Inmediate 8 bits
                    elif int(op_value) in range (0, 65536):                     #If the value of the operator is between 0 and 65335
                        return 2                                            #The operator is Inmediate 16 bits
                    else:
                        print "O_ERROR: Operador fuera del rango valido - 8bits [0-255] - 16bits [0-65535]"
                        return None
                elif int(op_value) in range (0, 256):                           #If the value of the operator is between 0 and 255 and doesn't have a #
                    return 3                                                    #The operator is Direct
                elif int(op_value) in range (256, 65536):                       #If the value of the operator is between 0 and 65535 and doesn't have a #
                    return 4                                                    #The operator is Extended
                else:
                    print "O_ERROR: Operador fuera del rango valido - Directo [0 - 256] Extendido [256 - 65535]"
                    return None
            else:
                return None
        print "\n"
        return True                                                     #Return True to keep working

    def get_value(self):                                                #This function get the numerical value in decimal base of the operator
        base = base_evaluation(self.value)                              #The variable "base" takes a numeric value between 0 and 4
        if self.value == None:
            return False
        if self.value[0] == '#':                                        #If the operator's first character is a "#"
            op_value = self.value[1:]                                   #Rewrite the local variable "op_value" without the "#"
        else:                                                           #If the operator doesn't have a "#"
            op_value = self.value                                       #Rewrite the local variable "op_value"

        if base == 1:                                                   #if the base is '1' the operator is decimal
            op_value = int(op_value)                                    #The variable "op_value" takes the value of the operator casted to Integer
        
        elif base == 2:                                                 #If the base is '2' the operator is binary
            op_value = op_value[1:]                                     #Rewrite the local variable "op_value" without the '%'
            if op_value != "":
                flag = False
                for x in op_value:
                    if x in binary:
                        flag = True
                    else:
                        flag = False
                        break

                if flag == True:
                    op_value = "0b" + str(op_value)                             #Rewrite the local variable "op_value" with the string "0b" at the beginning
                    op_value = int(op_value, 2)                                 #The variable "op_value" takes the value of the operator casted to Integer from Binary
                else:
                    print "O_ERROR: Los caracteres del operando son incorrectos, caracteres validos - [0,1]\n"
                    return False
            else:
                print "O_ERROR: El valor del operando en binario esta incompleto"
                return False

        elif base == 3:                                                 #If the base is '3' the operator is octal
            op_value = op_value[1:]                                     #Rewrite the local variable "op_value" without the '@'
            if op_value != "":
                flag = False
                for x in op_value:
                    if x in octal:
                        flag = True
                    else:
                        flag = False
                        break
                if flag == True:
                    op_value = int(op_value, 8)                                 #The variable "op_value" takes the value of the operator casted to Integer from Octal
                else:
                    print "O_ERROR: Los caracteres del operando son incorrectos, caracteres validos - [0-7]\n"
                    return False
            else:
                print "O_ERROR: El valor del operando en octal esta incompleto"
                return False

        elif base == 4:                                                 #if the base is '4' the operator is hexadecimal
            op_value = op_value[1:].upper()                                     #Rewrite the local variable "op_value" without the '$'
            if op_value != "":
                flag = False
                for x in op_value:
                    if x in hexadecimal:
                        flag = True
                    else:
                        flag = False
                        break
                if flag == True:
                    op_value = "0x" + op_value                                  #Rewrite the local variable "op_value" with the string "0x" at the beginning
                    op_value = int(op_value, 16)                                #The variable "op_value" takes the value of the operator casted to Integer from Hexadecimal
                else:
                    print "O_ERROR: Los caracteres del operando son incorrectos, caracteres validos - [0-9 A-F]\n"
                    return False
            else:
                print "O_ERROR: El valor del operando en hex esta incompleto"
                return False
        else:                                                           #If the base is not a valid base
            print "O_Error: El valor del operando no tiene una base valida\n"   #Print error
            return False
        return op_value                                                 #Return the value casted