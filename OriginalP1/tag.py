#!/usr/bin/python2.7
import re

class Tag:
	regex = re.compile('^[a-zA-Z][\_a-zA-Z0-9]{0,7}$')

	def __init__(self, s=""):
		self.value = s

	def validator(self):
		r = Tag.regex.match(self.value)

		if r != None:
			return True	
		else:
			if len(self.value) > 8:
				print "ERROR: Tag with lenght > 8 characters"

			if not self.value[0].isalpha():
				print "ERROR: First character must be an alphabetic character"
			
			for char in self.value:
				if not char.isalnum() and not char == "_":
					print "ERROR: " + char  + " is not an alphanumeric character"

			return False
