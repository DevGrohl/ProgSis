from tag import *
from codop import *
from operator import *

class Line:
	t = Tag()
	c = Codop()
	o = Operator()

	def __init__(self, s):
		self.list = s


	def tokenizer(self):
		line = self.list.split()

		if self.list[0] == ";":
			print "COMENTARIO" + "\n"
		else:
			if len(line) == 3:
				self.t.value = line[0]
				self.c.value = line[1]
				self.o.value = line[2]
				if self.t.validator() and self.c.validator() and self.o.validator:
					print "ETIQUETA: " + self.t.value
					print "CODOP   : " + self.c.value
					print "OPERANDO: " + self.o.value + "\n"
			elif len(line) == 2:
				if self.list[0] == " " or self.list[0] == "\t":
					self.c.value = line[0]
					self.o.value = line[1]
					if self.c.validator() and self.o.validator():
						print "ETIQUETA : NULL"
						print "CODOP    : " + self.c.value
						print "OPERANDO : " + self.o.value + "\n"
				else:
					self.t.value = line[0]
					self.c.value = line[1]
					if self.t.validator() and self.c.validator():
						print "ETIQUETA : " + self.t.value
						print "CODOP    : " + self.c.value 
						print "OPERANDO : NULL\n"
			else:
				self.c.value = line[0]
				if self.c.validator():
					print "ETIQUETA : NULL"
					print "CODOP   : " + self.c.value
					print "OPERANDO : NULL\n"	