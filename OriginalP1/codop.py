#!/usr/bin/python2.7
import re

class Codop:
	regex = re.compile('^[a-zA-Z]([a-zA-Z]{0,4}|[\.]{0,1}[a-zA-Z]{0,3}|[a-zA-Z]{0,1}[\.]{0,1}[a-zA-Z]{0,2}|[a-zA-Z]{0,3}[\.]{0,1}|[a-zA-Z]{0,2}[\.]{0,1}[a-zA-Z]{0,1})$')

	def __init__(self, s = ""):
		self.value = s

	def validator(self):
		r = Codop.regex.match(self.value)

		if r != None:
			return True
		else:
			print "Wrong codop: " + self.value			
			if len(self.value) > 5:
				print "ERROR: Codop with lenght > 5 characters"

			if not self.value[0].isalpha():
				print "ERROR: First character must be an alphabetic character"
			
			for char in self.value:
				if not char.isalnum() and not char == ".":
					print "ERROR: " + char  + " is not an alphanumeric character"

			return False

