#!/usr/bin/python2.7
import re
from tabop import *

class Codop:
	regex = re.compile('^[a-zA-Z]([a-zA-Z]{0,4}|[\.]{0,1}[a-zA-Z]{0,3}|[a-zA-Z]{0,1}[\.]{0,1}[a-zA-Z]{0,2}|[a-zA-Z]{0,3}[\.]{0,1}|[a-zA-Z]{0,2}[\.]{0,1}[a-zA-Z]{0,1})$')
	#Begins with a  UPPER or lower case letter
	#Only one dot (.) per codop, but not at the beginning
	#Max lenght its 5 characters

	def __init__(self, s = ""):
		self.value = s

	def validator(self):
		r = Codop.regex.match(self.value) 											#Regex.match() returns NONE if the value pass the regex

		if r != None:																#If the codop passed...
			return True																	#Return true
		else:																		#Else:
			print "Wrong codop: " + self.value											
			if len(self.value) > 5:														#If the lenght its bigger than 5 characters
				print "ERROR: Codop with lenght > 5 characters\n"

			if not self.value[0].isalpha():												#If the first character wasn't a letter
				print "ERROR: First character must be an alphabetic character"
			
			for char in self.value:														#for each character in the string
				if not char.isalpha() and not char == ".":									#if it wasn't a letter or a dot(.)
					print "ERROR: " + char  + " is not an alphabetic character\n"

			return False

	def evaluator(self):
		tabop = Tabop()
		tabop.readTabop()
		lines = []
		# print self.value
		for line in tabop.tabopData:
			if line[0] == self.value.upper():
				lines.append(line)

		return lines