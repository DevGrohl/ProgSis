from tag import *
from codop import *
from operator import *

class Line:
	t = Tag()
	c = Codop()
	o = Operator()

	def __init__(self, s):
		self.line = s
		self.tokens = []

	def resetVariables(self):
		self.t.value = None
		self.c.value = None
		self.o.value = None

	def tokenizer(self):
		checker = self.line.splitlines(True) 													#Returns a list with linebreaks and strings
		self.tokens = self.line.split() 														#List of strings without linebreaks
		# print self.tokens
		# print self.line
		if ((self.line[0] == ' ' or self.line[0] == '\t') and len(self.tokens) > 2) or len(self.tokens) > 3 :
			string = ""
			for key in self.tokens[1:]:
				string += key+ " "
				# print string
				self.tokens.remove(key)
			self.tokens.append(string)

		# print self.tokens	

		if checker == []:																		#If there is an empty line
			# print "Lista vacia"																#Print it! or pass
			pass
		else:
			if self.tokens == []:																#If there is an empty token
				print "Los tokens no contienen caracteres alfabeticos\n"
			else:
				if checker[0][0] == ' ' or checker[0][0] == '\t' or checker[0][0] == '\r': 		# Checker [0][0] is the first letter of the first string in the line
																								#if that first letter is a space, tabulation or return then the first token is not a tag
					if len(self.tokens) == 2:														#If there are TWO tokens
						self.c.value = self.tokens[0]													#The first one is a codop
						self.o.value = self.tokens[1]													#The second one is an operator
						if self.c.validator() and self.o.validator():									#If both tokens pass throught their validators:
							print "ETIQUETA : NULL"	
							print "CODOP    : " + self.c.value
							print "OPERANDO : " + self.o.value + "\n"
					elif len(self.tokens) == 1:														#If there is only ONE token
						self.c.value = self.tokens[0]													#Then that token is a codop
						if self.c.validator():															#If the codop pass his validator
							print "ETIQUETA : NULL"
							print "CODOP   : " + self.c.value
							print "OPERANDO : NULL\n"
					else:																			#If there ammount of tokens is not TWO or ONE
						print "ERROR: Numero de argumentos incorrectos\n"		
					if self.c.value == "END":														#If the codop is END...
						exit(0)																			#Finish the script
				else:																			#If the first letter wasn't a space, tabulation or return:
					if self.tokens[0] == "END":													#if the tag was END is not valid since END must be a codop
						print "ERROR: Codop invalido\n"

					if len(self.tokens) == 2:													#If there are TWO tokens...
						self.t.value = self.tokens[0]												#The first one is a tag
						self.c.value = self.tokens[1]												#The second one is a codop
						if self.t.validator() and self.c.validator():								#If both tokens pass throught their validators:
							print "ETIQUETA : " + self.t.value
							print "CODOP    : " + self.c.value 
							print "OPERANDO : NULL\n"	
					elif len(self.tokens) == 3:													#If there are THREE tokens...
						self.t.value = self.tokens[0]												#The first one is a tag
						self.c.value = self.tokens[1]												#The second one is a codop
						self.o.value = self.tokens[2]												#The third one is an operator
						if self.t.validator() and self.c.validator() and self.o.validator:			#If all the tokens pass throught their validators:
							print "ETIQUETA: " + self.t.value
							print "CODOP   : " + self.c.value
							print "OPERANDO: " + self.o.value + "\n"
					else:																		#If the ammount of tokens isn't TWO or THREE
						print "ERROR: Numero de argumentos incorrectos\n"


				tabop = self.c.evaluator()

				if tabop != []:
					# print tabop, "\n"
					if tabop[0][1] == "NO" and self.o.value != None:
						print "Error: Codop contiene operador que no deberia\n"
					elif tabop[0][1] == "SI" and self.o.value == None:
						print "Error: Codop no contiene operador que deberia\n"
					else:
						for reg in tabop:
							print "Modo de direccionamiento: ", reg[2]
							print "Codigo maquina calculado: ", reg[3]
							print "Total de bytes calculado: ", reg[4]
							print "Total de bytes no calculados: ", reg[5]
							print "Suma total de bytes: ", reg[6]
						print ""
				else:
					print "Error, codop no encontrado\n"
				# print self.c.value.upper()
				if self.c.value.upper() == "END":
					return 1
				self.resetVariables()
