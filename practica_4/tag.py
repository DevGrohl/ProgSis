#!/usr/bin/python2.7
import re

class Tag:
    regex = re.compile('^[a-zA-Z][\_a-zA-Z0-9]{0,7}$')
    #Begins with UPPER or lower case letter
    #This first letter must be at the beggining of the string
    #Max lenght its 8 characters
    #After the first letter it can be letters, digits or _
    def __init__(self, s=""):
        self.value = s

    def validator(self):
        r = Tag.regex.match(self.value)

        if r != None:
            return True 
        else:
            if len(self.value) > 8:
                print "T_ERROR: Tag with lenght > 8 characters"

            if not self.value[0].isalpha():
                print "T_ERROR: First character must be an alphabetic character"
            
            for char in self.value:
                if not char.isalnum() and not char == "_":
                    print "T_ERROR: " + char  + " is not an alphanumeric character"

            return False
